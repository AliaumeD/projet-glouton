//
// Created by Aliaume on 07/01/2021.
//

#include "DataReader.h"

vector<int> DataReader::reader(string file, vector<int> v) {
    string line;
    ifstream myfile("../src/"+file);

    if (myfile.is_open()){
        while (getline(myfile,line)){
            istringstream str(line);
            int n;
            while(str >> n){
                v.push_back(n);
            }
        }
        myfile.close();
    }
    else{
        cout << "Unable to open file" << endl;
    }

    return v;
}
