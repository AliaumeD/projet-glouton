//
// Created by Aliaume on 07/01/2021.
//

#ifndef PROJET_GLOUTON_DATAREADER_H
#define PROJET_GLOUTON_DATAREADER_H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

class DataReader {
public:
    vector<int> reader(string file, vector<int> v);
};


#endif //PROJET_GLOUTON_DATAREADER_H
