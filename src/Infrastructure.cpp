//
// Created by Aliaume on 07/01/2021.
//

#include "Infrastructure.h"

Infrastructure::Infrastructure(int i_width, int i_length,int index, string label) {
    this->i_width=i_width;
    this->i_length=i_length;
    this->label=label;
    build=false;
}

int Infrastructure::getILength() const{
    return i_length;
}

int Infrastructure::getIWidth() const{
    return i_width;
}


const string &Infrastructure::getLabel() const{
    return label;
}

void Infrastructure::setILength(int iLength) {
    i_length = iLength;
}

void Infrastructure::setIWidth(int iWidth) {
    i_width = iWidth;
}


void Infrastructure::setLabel(const string &label) {
    Infrastructure::label = label;
}

int Infrastructure::getIndex() const {
    return index;
}

void Infrastructure::setIndex(int index) {
    Infrastructure::index = index;
}

bool Infrastructure::isBuild() const {
    return build;
}

void Infrastructure::setBuild(bool build) {
    Infrastructure::build = build;
}
