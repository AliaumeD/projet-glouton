//
// Created by Aliaume on 07/01/2021.
//

#ifndef PROJET_GLOUTON_INFRASTRUCTURE_H
#define PROJET_GLOUTON_INFRASTRUCTURE_H
#include <string>
#include <vector>
using namespace std;

class Infrastructure {
private:
    int i_length;
    int i_width;
    int index;
public:
    bool isBuild() const;

    void setBuild(bool build);

private:
    bool build;
    string label;

public:
    Infrastructure(int i_width, int i_length,int index,string label);

    int getILength() const;

    int getIWidth() const;

    int getIndex() const;

    void setIndex(int index);

    void setILength(int iLength);

    void setIWidth(int iWidth);

    void setLabel(const string &label);

    const string &getLabel() const;

};


#endif //PROJET_GLOUTON_INFRASTRUCTURE_H
