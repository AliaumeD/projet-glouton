//
// Created by Aliaume on 07/01/2021.
//

#include "Map.h"

Map::Map(int m_width,int m_length, int nb_buildings){
    this->m_width=m_width;
    this->m_length=m_length;
    this->nb_buildings=nb_buildings;
    map = new int*[m_length];
    for(int i=0;i<m_width;i++)
    {
        map[i]=new int[m_width];
    }
    for(int i=0;i<m_length;i++) {
        for (int j=0; j <m_width ; j++) {
            map[i][j]=0;
        }
    }
}

int Map::getMWidth() const{
    return m_width;
}

int Map::getMLength() const{
    return m_length;
}

void Map::showMap() {
    for(int i=0;i<m_width;i++) {
        for (int j=0; j < m_length; j++) {
            if(map[i][j]>9)
                 cout<<map[i][j]<<" ";
            else
                cout<<map[i][j]<<"  ";

        }
        cout<<endl;
    }
}

bool Map::checkLocation(Infrastructure building,int xFirst,int yFirst) {
    int x= xFirst;
    int y= yFirst;
    if(!(x+building.getIWidth() <=m_width && y+building.getILength()<=m_length))//Si on sort du terrain -> false
    {
        return false;
    }
    for(int i = x; i < x+building.getIWidth(); i++){
        for(int j = y; j < y+building.getILength(); j++){
            if (map[i][j] != 0) {                                               //Si dans l'aire de notre bâtiment on rencontre une case occupée -> false
                return false;

            }
        }
    }
    return true;//Sinon -> true
}

void Map::putBuilding(Infrastructure & building,int xFirst,int yFirst) {
            if(checkLocation(building,xFirst,yFirst)) {                         //Si la fonction Check location est vérifié -> On place le bâtiment
                for (int k=xFirst; k < xFirst+building.getIWidth(); k++) {
                    for (int l=yFirst; l < yFirst+building.getILength(); l++) {
                        map[k][l] = building.getIndex();
                    }
                }
                building.setBuild(true);            }
}

void Map::putMairie(Infrastructure mairie) {                                    //On place la mairie en 0 0
    for(int i=0;i<mairie.getIWidth();i++){
        for(int j=0;j<mairie.getILength();j++) {
            map[i][j]=1;
        }
    }
}

void Map::sortByArea(vector<Infrastructure> & buildings) {                      //Tri par aire décroissante
    for(int i=0;i<buildings.size();i++) {
        for (int i = 0; i < buildings.size(); i++) {
            if (i > 0) {
                if (buildings[i].getILength() * buildings[i].getIWidth() >buildings[i - 1].getILength() * buildings[i - 1].getIWidth()) {
                    swap(buildings[i - 1], buildings[i]);

                }
            }
        }
    }
}

void Map::sortByWidthMoreLength(vector<Infrastructure> & buildings) {          //Tri par encombrement (longueur + largeur)
    for(int i=0;i<buildings.size();i++) {
        for (int i = 0; i < buildings.size(); i++) {
            if (i > 0) {
                if (buildings[i].getILength() + buildings[i].getIWidth() >buildings[i - 1].getILength() + buildings[i - 1].getIWidth()) {
                    swap(buildings[i - 1], buildings[i]);

                }
            }
        }
    }
}

void Map::randomSort(vector<Infrastructure> &buildings) {                       //Tri aléatoire
    for(int i=0;i<buildings.size();i++) {
        for (int i = 0; i < buildings.size(); i++) {
            if (i > 0) {
                int cpt=rand()%2;
                if (cpt==0) {
                    swap(buildings[i - 1], buildings[i]);

                }
            }
        }
    }
}

void Map::showScore() {
    int score=0;
    for(int i=0;i<m_width;i++) {
        for (int j=0; j < m_length; j++) {
            if(map[i][j]==0)
                score++;
        }
    }
    cout<<"Le score est de : "<<m_length*m_width-score<<"/"<<m_length*m_width;
}



