//
// Created by Aliaume on 07/01/2021.
//

#ifndef PROJET_GLOUTON_MAP_H
#define PROJET_GLOUTON_MAP_H
using namespace std;
#include <iostream>
#include "Infrastructure.h"
#include <vector>
#include <random>

class Map {
private:
    int **map;
    int m_width;
    int m_length;
    int nb_buildings;

public:
    Map(int m_width,int m_length,int nb_buildings);
    int getMWidth() const;

    int getMLength() const;

    bool checkLocation(Infrastructure building,int x,int y);

    void showMap();

    void putMairie(Infrastructure mairie);

    void putBuilding(Infrastructure & building, int x, int y);

    void sortByArea(vector<Infrastructure> & buildings);

    void sortByWidthMoreLength(vector<Infrastructure> & buildings);

    void randomSort(vector<Infrastructure> & buildings);

    void showScore();
};


#endif //PROJET_GLOUTON_MAP_H
