#include <iostream>
#include "Infrastructure.h"
#include "Map.h"
#include "DataReader.h"
int main() {
    vector<int> data;
    DataReader d;
    vector<Infrastructure> building_list;
    data = d.reader("instance01.txt",data);                                          //On lit notre fichier instance
    Map m(data[0],data[1],data[2]);
    Infrastructure mairie(data[3],data[4],1,"mairie");          //On initialise notre maire
    Infrastructure tmp(0, 0,0, "building");
    int cpt=1;                                                                           //La boucle qui suit permet d'ajouter nos bâtiments à notre vecteur  de bâtiment
    for (int i=3; i<data.size(); i++) {
        if (i % 2 != 0) {
            tmp.setIWidth(data[i]);
        } else {
            cpt++;
            tmp.setILength(data[i]);
            tmp.setIndex(cpt);
            building_list.push_back(tmp);
            Infrastructure tmp(0, 0,0,  "building");
        }
    }
    m.sortByArea(building_list);                        //On choisit parmi l'un des trois tris
    //m.sortByWidthMoreLength(building_list);
    //m.randomSort(building_list);

    m.putMairie(mairie); // On place la mairie

    for(int k=0;k<building_list.size();k++){                //On place les bâtiments
        for(int i=0;i<m.getMWidth();i++) {
            for (int j = 0; j < m.getMLength(); j++) {
                    if(!building_list[k].isBuild()){
                        m.putBuilding(building_list[k],i,j);
                    }
            }
        }
    }
    //On affiche le terrain et le score
    m.showMap();
    m.showScore();
}